routes.push({
    path:"/qina",
    meta:{
        title:"齐纳二极管计算器"
    },
    component:{
        data:function(){
            return {
                vimax:12,
                vimin:11,
                vo:10,
                io:0.01,
                r:0,
                rp:0,
                z:0,
                zp:0
            }
        },
        created:function(){
            this.update();
        },
        methods:{
            update:function(){
                var vimax=this.vimax;
                var vimin=this.vimin;
                var vo=this.vo;
                var io=this.io;
                if((vo+0.8)>vimin){
                    window.Mtu.dialog({title:'错误',message:'最小输入电压过低或输出电压选定过高！',positive:'确定'});
                }else{
                    var r=(vimin-vo)/(io+0.01);
                    this.r=r;
                    this.rp=Math.pow((vimax-vo),2)/r;
                    this.z=vo;
                    var it=(vimax-vo)/r;
                    this.zp=vo*it;
                };
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">齐纳二极管计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2">
                                    <img style="width:50%" src="images/qina.png">
                                </td>
                            </tr>
                            <tr>
                                <td>最大输入电压：</td>
                                <td>
                                    <mtu-phyv unit="V" default="0" v-model="vimax"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>最小输入电压：</td>
                                <td>
                                    <mtu-phyv unit="V" default="0" v-model="vimin"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>输出电压：</td>
                                <td>
                                    <mtu-phyv unit="V" default="0" v-model="vo"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>负载电流：</td>
                                <td>
                                    <mtu-phyv unit="A" default="-3" v-model="io"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="update()">计算</m-button>
                                </td>
                            </tr>
                            <tr>
                                <td>限流电阻：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="r"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>电阻功率：</td>
                                <td>
                                    <mtu-phyv unit="W" default="0" v-model="rp"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>二极管电压：</td>
                                <td>
                                    <mtu-phyv unit="V" default="0" v-model="z"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>二极管功率：</td>
                                <td>
                                    <mtu-phyv unit="W" default="0" v-model="zp"></mtu-phyv>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});