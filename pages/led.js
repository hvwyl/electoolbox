routes.push({
    path:"/led",
    meta:{
        title:"LED限流电阻阻值计算器"
    },
    component:{
        data:function(){
            return {
                type:1,

                e1:12,
                u1:2.2,
                i1:0.01,
                r1:0,
                p1:0,

                e2:12,
                u2:2.2,
                i2:0.01,
                n2:2,
                r2:0,
                p2:0,

                e3:12,
                u3:2.2,
                i3:0.01,
                n3:2,
                r3:0,
                p3:0,
            }
        },
        created:function(){
            this.update1();
            this.update2();
            this.update3();
        },
        methods:{
            update1:function(){
                var e1=this.e1;
                var u1=this.u1;
                var i1=this.i1;
                if(e1<u1 ||i1==0){
                    window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'});
                }else{
                    this.r1=(e1-u1)/i1;
                    this.p1=(e1-u1)*i1;
                };
            },
            update2:function(){
                var e2=this.e2;
                var u2=this.u2;
                var i2=this.i2;
                var n2=this.n2;
                if(e2<u2*n2 ||i2==0){
                    window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'});
                }else{
                    this.r2=(e2-u2*n2)/i2;
                    this.p2=(e2-u2*n2)*i2;
                };
            },
            update3:function(){
                var e3=this.e3;
                var u3=this.u3;
                var i3=this.i3;
                var n3=this.n3;
                if(e3<u3 ||i3*n3==0){
                    window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'});
                }else{
                    this.r3=(e3-u3)/(i3*n3);
                    this.p3=(e3-u3)*(i3*n3);
                };
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">LED限流电阻阻值计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <div>注：不同厂家，不同颜色压降是不一样的.</div>
                        <table style="width:100%">
                            <tr>
                                <td>红色：</td><td>2.0-2.2 V</td>
                                <td>黄色：</td><td>1.8-2.0 V</td>
                            </tr>
                            <tr>
                                <td>白色：</td><td>3.0-4.0 V</td>
                                <td>蓝色：</td><td>3.0-4.0 V</td>
                            </tr>
                            <tr>
                                <td>绿色：</td>
                                <td colspan="3">2.0-2.2V 3.0-3.2V</td>
                            </tr>
                        </table>
                        <div>以上仅供参考.</div>
                    </m-card>
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <m-tab :select="type-1" style="background-color:white">
                            <m-tab-item @click="type=1" style="text-transform:none">单只LED电路</m-tab-item>
                            <m-tab-item @click="type=2" style="text-transform:none">LED串联电路</m-tab-item>
                            <m-tab-item @click="type=3" style="text-transform:none">LED并联电路</m-tab-item>
                        </m-tab>
                        <div v-show="type==1?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td colspan="2">
                                        <img style="width:50%" src="images/led_1.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td>电源电压：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="e1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED压降：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="u1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED电流：</td>
                                    <td>
                                        <mtu-phyv unit="A" default="-3" v-model="i1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <m-button @click="update1()">计算</m-button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>限流电阻：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="0" v-model="r1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>电阻功率：</td>
                                    <td>
                                        <mtu-phyv unit="W" default="0" v-model="p1"></mtu-phyv>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div v-show="type==2?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td colspan="2">
                                        <img style="width:50%" src="images/led_2.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td>电源电压：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="e2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED压降：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="u2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED电流：</td>
                                    <td>
                                        <mtu-phyv unit="A" default="-3" v-model="i2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED数量：</td>
                                    <td>
                                        <m-edit-input :value="n2" @input="n2=parseInt($event.target.value)" style="width:100%"></m-edit-input>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <m-button @click="update2()">计算</m-button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>限流电阻：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="0" v-model="r2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>电阻功率：</td>
                                    <td>
                                        <mtu-phyv unit="W" default="0" v-model="p2"></mtu-phyv>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div v-show="type==3?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td colspan="2">
                                        <img style="width:80%" src="images/led_3.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td>电源电压：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="e3"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED压降：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="u3"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED电流：</td>
                                    <td>
                                        <mtu-phyv unit="A" default="-3" v-model="i3"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>LED数量：</td>
                                    <td>
                                        <m-edit-input :value="n3" @input="n3=parseInt($event.target.value)" style="width:100%"></m-edit-input>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <m-button @click="update3()">计算</m-button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>限流电阻：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="0" v-model="r3"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>电阻功率：</td>
                                    <td>
                                        <mtu-phyv unit="W" default="0" v-model="p3"></mtu-phyv>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});