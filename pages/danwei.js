routes.push({
    path:"/danwei",
    meta:{
        title:"电阻、电容、电感单位换算"
    },
    component:{
        data:function(){
            return {
                r:0,
                c:0,
                l:0
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">电阻、电容、电感单位换算</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <div>电阻</div>
                        <mtu-phyv v-model="r" unit="Ω" default="0"></mtu-phyv>
                        <mtu-phyv v-model="r" unit="Ω" default="0"></mtu-phyv>
                    </m-card>
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <div>电容</div>
                        <mtu-phyv v-model="c" unit="F" default="-6"></mtu-phyv>
                        <mtu-phyv v-model="c" unit="F" default="-6"></mtu-phyv>
                    </m-card>
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <div>电感</div>
                        <mtu-phyv v-model="l" unit="H" default="-3"></mtu-phyv>
                        <mtu-phyv v-model="l" unit="H" default="-3"></mtu-phyv>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});