routes.push({
    path:"/5sehuan",
    meta:{
        title:"五色环电阻阻值计算器"
    },
    component:{
        data:function(){
            return {
                first_band:0,
                second_band:0,
                third_band:0,
                multiplier_band:1,
                tolerance_band:0.01
            }
        },
        computed:{
            total_resistance:function(){
                return parseFloat(((parseInt(this.first_band)*100+parseInt(this.second_band)*10+parseInt(this.third_band))*parseFloat(this.multiplier_band)).toFixed(5));
            },
            total_tolerance:function(){
                return parseFloat((parseFloat(this.total_resistance)*parseFloat(this.tolerance_band)).toFixed(5));
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">五色环电阻阻值计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom: 16px">
                        <table style="margin: 0 auto">
                            <tr>
                                <td>电阻：</td>
                                <td>{{total_resistance}} 欧姆</td>
                            </tr>
                            <tr>
                                <td>误差±：</td>
                                <td>{{total_tolerance}} 欧姆</td>
                            </tr>
                        </table>
                    </m-card>
                    <table style="margin: 0 auto">
                        <tr>
                            <td>第一环</td>
                            <td><colorselect v-model="first_band"></colorselect></td>
                        </tr>
                        <tr>
                            <td>第二环</td>
                            <td><colorselect v-model="second_band"></colorselect></td>
                        </tr>
                        <tr>
                            <td>第三环</td>
                            <td><colorselect v-model="third_band"></colorselect></td>
                        </tr>
                        <tr>
                            <td>第四环(乘数)</td>
                            <td><colorselect v-model="multiplier_band" multiplier="true"></colorselect></td>
                        </tr>
                        <tr>
                            <td>第五环(误差)</td>
                            <td><toleranceselect v-model="tolerance_band"></toleranceselect></td>
                        </tr>
                    </table>
                </div>
            </m-scrollbar>
        </div>`,
        components:{
            colorselect:{
                props:["value","multiplier"],
                data:function(){
                    return {
                        bind:0,
                        btnStyle:{
                            "background-color":"#000000",
                            "min-width":"36px"
                        }
                    }
                },
                created:function(){
                    if(this.multiplier){
                        this.bind=(function(value){
                            if(value>=0){
                                return value;
                            }else{
                                return 9-value;
                            };
                        })(Math.log10(this.value));
                    }else{
                        this.bind=this.value;   
                    };
                    this.update();
                },
                watch:{
                    value:function(){
                        if(this.multiplier){
                            this.bind=(function(value){
                                if(value>=0){
                                    return value;
                                }else{
                                    return 9-value;
                                };
                            })(Math.log10(this.value));
                        }else{
                            this.bind=this.value;   
                        };
                        this.update();
                    },
                    bind:function(){
                        this.update();
                    }
                },
                methods:{
                    update:function(){
                        if(this.multiplier){
                            var colorSet=["#000000","#804000","#ff0000","#ffa000","#ffff00","#348017","#0000ff","#cc3399","#b9b7b7","#ffffff","#cc9900","#efefef"];
                            this.btnStyle["background-color"]=colorSet[this.bind];
                            this.value=Math.pow(10,(function(bind){
                                if(bind<=9){
                                    return bind;
                                }else{
                                    return 9-bind;
                                };
                            })(this.bind));
                            this.$emit('input', this.value);
                        }else{
                            var colorSet=["#000000","#804000","#ff0000","#ffa000","#ffff00","#348017","#0000ff","#cc3399","#b9b7b7","#ffffff"];
                            this.btnStyle["background-color"]=colorSet[this.bind];
                            this.value=this.bind;
                            this.$emit('input', this.value);
                        };
                    }
                },
                template:`<div style="display:inline">
                    <m-spinner :select="bind" @change="bind=$event.target.select" style="width:80px">
                        <m-spinner-item style="background-color: #000000;color: #ffffff;">黑</m-spinner-item>
                        <m-spinner-item style="background-color: #804000;color: #ffffff;">棕</m-spinner-item>
                        <m-spinner-item style="background-color: #ff0000;color: #ffffff;">红</m-spinner-item>
                        <m-spinner-item style="background-color: #ffa000;color: #000000;">橙</m-spinner-item>
                        <m-spinner-item style="background-color: #ffff00;color: #000000;">黄</m-spinner-item>
                        <m-spinner-item style="background-color: #348017;color: #ffffff;">绿</m-spinner-item>
                        <m-spinner-item style="background-color: #0000ff;color: #ffffff;">蓝</m-spinner-item>
                        <m-spinner-item style="background-color: #cc3399;color: #ffffff;">紫</m-spinner-item>
                        <m-spinner-item style="background-color: #b9b7b7;color: #000000;">灰</m-spinner-item>
                        <m-spinner-item style="background-color: #ffffff;color: #000000;">白</m-spinner-item>
                        <template v-if="multiplier">
                            <m-spinner-item style="background-color: #cc9900;color: #000000;">金</m-spinner-item>
                            <m-spinner-item style="background-color: #efefef;color: #000000;">银</m-spinner-item>
                        </template>
                    </m-spinner>
                    <m-button :style="btnStyle"></m-button>
                    <m-button style="width:100px">{{ multiplier?"X":"" }}{{value}}</m-button>
                </div>`
            },
            toleranceselect:{
                props:["value"],
                data:function(){
                    return {
                        bind:0,
                        btnStyle:{
                            "background-color":"#000000",
                            "min-width":"36px"
                        }
                    }
                },
                created:function(){
                    var bindSet={0.01:0,0.02:1,0.03:2,0.05:3,0.1:4};
                    this.bind=bindSet[this.value];
                    this.update();
                },
                watch:{
                    value:function(){
                        var bindSet={0.01:0,0.02:1,0.03:2,0.05:3,0.1:4};
                        this.bind=bindSet[this.value];
                        this.update();
                    },
                    bind:function(){
                        this.update();
                    }
                },
                methods:{
                    update:function(){
                        var colorSet=["#804000","#ff0000","#ffa000","#cc9900","#efefef"];
                        var valueSet=[0.01,0.02,0.03,0.05,0.1];
                        this.btnStyle["background-color"]=colorSet[this.bind];
                        this.value=valueSet[this.bind];
                        this.$emit('input', this.value);
                    }
                },
                template:`<div style="display:inline">
                    <m-spinner :select="bind" @change="bind=$event.target.select" style="width:80px">
                        <m-spinner-item style="background-color: #804000;color: #ffffff;">棕</m-spinner-item>
                        <m-spinner-item style="background-color: #ff0000;color: #ffffff;">红</m-spinner-item>
                        <m-spinner-item style="background-color: #ffa000;color: #000000;">橙</m-spinner-item>
                        <m-spinner-item style="background-color: #cc9900;color: #000000;">金</m-spinner-item>
                        <m-spinner-item style="background-color: #efefef;color: #000000;">银</m-spinner-item>
                    </m-spinner>
                    <m-button :style="btnStyle"></m-button>
                    <m-button style="width:100px">{{value*100}}%</m-button>
                </div>`
            }
        }
    }
});