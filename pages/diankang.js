routes.push({
    path:"/diankang",
    meta:{
        title:"电抗/谐振计算器"
    },
    component:{
        data:function(){
            return {
                xc1:0,
                c1:0.0001,
                f1:100,

                xl2:0,
                l2:0.1,
                f2:100,

                l3:0.1,
                c3:0.0001,
                f3:0
            }
        },
        created:function(){
            this.xc1=1/(2*Math.PI*this.f1*this.c1);
            this.xl2=2*Math.PI*this.f2*this.l2;
            this.f3=1/(2*Math.PI*Math.sqrt(this.l3*this.c3));
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">电抗/谐振计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2">容抗计算</td>
                            </tr>
                            <tr>
                                <td>Xc：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="xc1"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>C：</td>
                                <td>
                                    <mtu-phyv unit="F" default="-6" v-model="c1"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>频率：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="0" v-model="f1"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="if(f1==0 || c1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{xc1=1/(2*Math.PI*f1*c1)}">计算容抗</m-button>
                                    <m-button @click="if(f1==0 || xc1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{c1=1/(2*Math.PI*f1*xc1)}">计算电容</m-button>
                                    <m-button @click="if(c1==0 || xc1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{f1=1/(2*Math.PI*c1*xc1)}">计算频率</m-button>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2">感抗计算</td>
                            </tr>
                            <tr>
                                <td>Xl：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="xl2"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>L：</td>
                                <td>
                                    <mtu-phyv unit="H" default="-3" v-model="l2"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>频率：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="0" v-model="f2"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="xl2=2*Math.PI*f2*l2">计算感抗</m-button>
                                    <m-button @click="if(f2==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{l2=xl2/(2*Math.PI*f2)}">计算电感</m-button>
                                    <m-button @click="if(l2==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{f2=xl2/(2*Math.PI*l2)}">计算频率</m-button>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2">谐振计算</td>
                            </tr>
                            <tr>
                                <td>L：</td>
                                <td>
                                    <mtu-phyv unit="H" default="-3" v-model="l3"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>C：</td>
                                <td>
                                    <mtu-phyv unit="F" default="-6" v-model="c3"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>频率：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="0" v-model="f3"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="if(f3==0 || c3==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{l3=1/(4*Math.PI*Math.PI*c3*f3*f3)}">计算电感</m-button>
                                    <m-button @click="if(f3==0 || l3==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{c3=1/(4*Math.PI*Math.PI*l3*f3*f3)}">计算电容</m-button>
                                    <m-button @click="if(l3==0 || c3==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{f3=1/(2*Math.PI*Math.sqrt(l3*c3))}">计算频率</m-button>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});