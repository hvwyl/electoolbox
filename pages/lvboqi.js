routes.push({
    path:"/lvboqi",
    meta:{
        title:"RC/RL滤波器计算器"
    },
    component:{
        data:function(){
            return {
                r1:100,
                c1:0.00001,
                f1:0,

                r2:100,
                l2:0.1,
                f2:0
            }
        },
        created:function(){
            this.f1=1/(2*Math.PI*this.r1*this.c1);
            this.f2=this.r2/(2*Math.PI*this.l2);
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">RC/RL滤波器计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2">一阶RC滤波器</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img style="width:100%" src="images/lvboqi_1.png">
                                </td>
                            </tr>
                            <tr>
                                <td>R：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="r1"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>C：</td>
                                <td>
                                    <mtu-phyv unit="F" default="-6" v-model="c1"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>频率：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="0" v-model="f1"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="if(f1==0 || c1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{r1=1/(2*Math.PI*f1*c1)}">计算电阻</m-button>
                                    <m-button @click="if(f1==0 || r1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{c1=1/(2*Math.PI*f1*r1)}">计算电容</m-button>
                                    <m-button @click="if(r1==0 || c1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{f1=1/(2*Math.PI*r1*c1)}">计算频率</m-button>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2">一阶RL滤波器</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img style="width:100%" src="images/lvboqi_2.png">
                                </td>
                            </tr>
                            <tr>
                                <td>R：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="r2"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>L：</td>
                                <td>
                                    <mtu-phyv unit="H" default="-3" v-model="l2"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>频率：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="0" v-model="f2"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="r2=2*Math.PI*f2*l2">计算电阻</m-button>
                                    <m-button @click="if(f2==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{l2=r2/(2*Math.PI*f2)}">计算电感</m-button>
                                    <m-button @click="if(l2==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{f2=r2/(2*Math.PI*l2)}">计算频率</m-button>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});