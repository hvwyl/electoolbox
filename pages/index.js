routes.push({
    path:"/",
    meta:{
        title:"电路计算工具箱"
    },
    component:{
        data:function(){
            return {
                menu:[
                    {link:"/5sehuan",name:"五色环电阻阻值计算器"},
                    {link:"/4sehuan",name:"四色环电阻阻值计算器"},
                    {link:"/danwei",name:"电阻、电容、电感单位换算"},
                    {link:"/ydelta",name:"Y-Δ变换计算器"},
                    {link:"/gonglv",name:"功率计算器"},
                    {link:"/diankang",name:"电抗/谐振计算器"},
                    {link:"/lvboqi",name:"RC/RL滤波器计算器"},
                    {link:"/dianrong",name:"电容时间常数计算器"},
                    {link:"/daitong",name:"带通滤波器设计计算器"},
                    {link:"/led",name:"LED限流电阻阻值计算器"},
                    {link:"/qina",name:"齐纳二极管计算器"},
                    {link:"/ne555",name:"NE555频率计算器"}
                ]
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <m-button slot="nav" mode="icon" theme="flat"
                    @click="window.Mtu.dialog({title:'提示',message:'电路计算工具箱 Alpha\\nBy HGC 2021.8.29',positive:'确定'})">
                        <m-icon value="help"></m-icon>
                    </m-button>
                    <div slot="title">电路计算工具箱</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <m-list>
                    <template v-for="item in menu">
                        <router-link :to="item.link" tag="div">
                            <m-list-item>
                                <div slot="title">{{item.name}}</div>
                            </m-list-item>
                        </router-link>
                    </template>
                </m-list>
            </m-scrollbar>
        </div>`
    }
});