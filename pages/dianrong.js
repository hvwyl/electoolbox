routes.push({
    path:"/dianrong",
    meta:{
        title:"电容时间常数计算器"
    },
    component:{
        data:function(){
            return {
                u:10,
                c:0.00001,
                r:100,
                t:0.001,
                e:0.0005,
                q:0.0001
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">电容时间常数计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td>电容电压：</td>
                                <td>
                                    <mtu-phyv unit="V" default="0" v-model="u"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>电容容量：</td>
                                <td>
                                    <mtu-phyv unit="F" default="-6" v-model="c"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>负载电阻：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="r"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="t=r*c;e=1/2*c*u*u;q=c*u">计算</m-button>
                                </td>
                            </tr>
                            <tr>
                                <td>时间常数：</td>
                                <td>
                                    <mtu-phyv unit="s" default="-3" v-model="t"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>能量：</td>
                                <td>
                                    <mtu-phyv unit="J" default="0" v-model="e"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>电荷量：</td>
                                <td>
                                    <mtu-phyv unit="C" default="0" v-model="q"></mtu-phyv>
                                </td>
                            </tr>
                        </table>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});