routes.push({
    path:"/daitong",
    meta:{
        title:"带通滤波器设计计算器"
    },
    component:{
        data:function(){
            return {
                n:1,
                c:1000000000,
                bw:1000000,
                z:50,
                r:0.01,
                out:{},

                type:1
            }
        },
        created:function(){
            this.update();
        },
        methods:{
            update:function(){
                this.out=this.calculate(this.n,this.c,this.bw,this.z,this.r);
            },
            calculate:function(n,c,bw,z,r){
                var c=c/1000000;
                var bw=bw/1000000;

                var w=2*Math.PI*c/1000;
                var wbw=2*Math.PI*bw/1000;
                var w2=Math.pow(w,2);

                var rr=r/17.37;
                var e2x=Math.exp(2*rr);
                var coth=(e2x+1)/(e2x-1);
                var bt=Math.log(coth);
                var btn=bt/(2*n);
                var gn=(Math.exp(btn)-Math.exp(-btn))/2;

                var a=new Array;
                var b=new Array;
                var g=new Array;
                var srl=new Array;
                var src=new Array;
                var shl=new Array;
                var shc=new Array;

                for(let i=1;i<=n;i++){
                    var k=(2*i-1)*Math.PI/(2*n);
                    a[i]=Math.sin(k);
                    var k2=Math.PI*i/n;
                    var k3=Math.sin(k2);
                    b[i]=Math.pow(gn,2)+Math.pow(k3,2);
                };

                g[1]=2*a[1]/gn;

                for(let i=2;i<=n;i++){
                    g[i]=(4*a[i-1]*a[i])/(b[i-1]*g[i-1]);
                };

                for(let i=1;i<=n;i++){
                    srl[i]=(g[i]*z/wbw)*Math.pow(10,-9);
                    src[i]=(wbw*1000/(g[i]*z*w2))*Math.pow(10,-12);
                    shl[i]=(wbw*z/(g[i]*w2))*Math.pow(10,-9);
                    shc[i]=(g[i]*1000/(wbw*z))*Math.pow(10,-12);
                };

               return {
                    type1:([
                        {L:shl[1],C:shc[1]},{L:srl[2],C:src[2]},{L:shl[3],C:shc[3]},{L:srl[4],C:src[4]},{L:shl[5],C:shc[5]},
                        {L:srl[6],C:src[6]},{L:shl[7],C:shc[7]},{L:srl[8],C:src[8]},{L:shl[9],C:shc[9]}
                    ]).splice(0,n),
                    type2:([
                        {L:srl[1],C:src[1]},{L:shl[2],C:shc[2]},{L:srl[3],C:src[3]},{L:shl[4],C:shc[4]},{L:srl[5],C:src[5]},
                        {L:shl[6],C:shc[6]},{L:srl[7],C:src[7]},{L:shl[8],C:shc[8]},{L:srl[9],C:src[9]}
                    ]).splice(0,n)
                };
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">带通滤波器设计计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <table style="width:100%">
                            <tr>
                                <td>连接路数：</td>
                                <td>
                                    <m-spinner :select="(n-1)/2" @change="n=2*($event.target.select+1)-1" style="width:100px">
                                        <m-spinner-item v-for="n in 5">{{2*n-1}}</m-spinner-item>
                                    </m-spinner>
                                </td>
                            </tr>
                            <tr>
                                <td>截止频率fc：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="6" v-model="c"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>通频带：</td>
                                <td>
                                    <mtu-phyv unit="Hz" default="6" v-model="bw"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>阻抗Zo：</td>
                                <td>
                                    <mtu-phyv unit="Ω" default="0" v-model="z"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td>波纹：</td>
                                <td>
                                    <mtu-phyv unit="dB" default="0" v-model="r"></mtu-phyv>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <m-button @click="update()">计算</m-button>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <div>
                            <img style="width:100%" src="images/daitong.png">
                        </div>
                        <m-tab :select="type-1" style="background-color:white">
                            <m-tab-item @click="type=1" style="text-transform:none">Type1</m-tab-item>
                            <m-tab-item @click="type=2" style="text-transform:none">Type2</m-tab-item>
                        </m-tab>
                        <div v-show="type==1?true:false">
                            <table style="width:100%">
                                <template v-for="(item,i) in out.type1">
                                    <tr>
                                        <td>L{{i+1}}：</td>
                                        <td>
                                            <mtu-phyv unit="H" default="-9" fix="64" :value="item.L"></mtu-phyv>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>C{{i+1}}：</td>
                                        <td>
                                            <mtu-phyv unit="F" default="-12" fix="64" :value="item.C"></mtu-phyv>
                                        </td>
                                    </tr>
                                </template>
                            </table>
                        </div>
                        <div v-show="type==2?true:false">
                            <table style="width:100%">
                                <template v-for="(item,i) in out.type2">
                                    <tr>
                                        <td>L{{i+1}}：</td>
                                        <td>
                                            <mtu-phyv unit="H" default="-9" fix="64" :value="item.L"></mtu-phyv>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>C{{i+1}}：</td>
                                        <td>
                                            <mtu-phyv unit="F" default="-12" fix="64" :value="item.C"></mtu-phyv>
                                        </td>
                                    </tr>
                                </template>
                            </table>
                        </div>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});