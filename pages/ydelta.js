routes.push({
    path:"/ydelta",
    meta:{
        title:"Y-Δ变换计算器"
    },
    component:{
        data:function(){
            return {
                mode:true,
                R1:300,
                R2:300,
                R3:300,
                Ra:900,
                Rb:900,
                Rc:900
            }
        },
        computed: {
            a:function(){
                return [this.R1,this.R2,this.R3];
            },
            b:function(){
                return [this.Ra,this.Rb,this.Rc];
            }
        },
        watch: {
            a:function(){
                this.update();
            },
            b:function(){
                this.update();
            }
        },
        methods:{
            update:function(){
                if(this.mode){
                    this.Ra=((this.R1*this.R2+this.R2*this.R3+this.R3*this.R1)/this.R2).toPrecision(5);
                    this.Rb=((this.R1*this.R2+this.R2*this.R3+this.R3*this.R1)/this.R1).toPrecision(5);
                    this.Rc=((this.R1*this.R2+this.R2*this.R3+this.R3*this.R1)/this.R3).toPrecision(5);
                }else{
                    this.R1=((this.Ra*this.Rc)/(this.Ra+this.Rb+this.Rc)).toPrecision(5);
                    this.R2=((this.Rc*this.Rb)/(this.Ra+this.Rb+this.Rc)).toPrecision(5);
                    this.R3=((this.Rb*this.Ra)/(this.Ra+this.Rb+this.Rc)).toPrecision(5);
                }
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">Y-Δ变换计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <div style="width:100%">
                        <img style="width:40%" src="images/ydelta_1.png">
                        <img style="width:40%" src="images/ydelta_2.png">
                    </div>
                    <table style="margin-bottom:16px;width:100%">
                        <tr>
                            <td>R1：</td>
                            <td>
                                <mtu-phyv unit="Ω" default="0" v-model="R1" @focus="mode=true"></mtu-phyv>
                            </td>
                        </tr>
                        <tr>
                            <td>R2：</td>
                            <td>
                                <mtu-phyv unit="Ω" default="0" v-model="R2" @focus="mode=true"></mtu-phyv>
                            </td>
                        </tr>
                        <tr>
                            <td>R3：</td>
                            <td>
                                <mtu-phyv unit="Ω" default="0" v-model="R3" @focus="mode=true"></mtu-phyv>
                            </td>
                        </tr>
                        <tr>
                            <td>Ra：</td>
                            <td>
                                <mtu-phyv unit="Ω" default="0" v-model="Ra" @focus="mode=false"></mtu-phyv>
                            </td>
                        </tr>
                        <tr>
                            <td>Rb：</td>
                            <td>
                                <mtu-phyv unit="Ω" default="0" v-model="Rb" @focus="mode=false"></mtu-phyv>
                            </td>
                        </tr>
                        <tr>
                            <td>Rc：</td>
                            <td>
                                <mtu-phyv unit="Ω" default="0" v-model="Rc" @focus="mode=false"></mtu-phyv>
                            </td>
                        </tr>
                    </table>
                </div>
            </m-scrollbar>
        </div>`
    }
});