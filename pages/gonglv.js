routes.push({
    path:"/gonglv",
    meta:{
        title:"功率计算器"
    },
    component:{
        data:function(){
            return {
                mode:1,
                v1:5,
                a1:2,
                p1:10,

                v2:220,
                a2:0.25,
                f2:0.8,
                p2:44,
                q2:33,
                s2:55
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">功率计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-spinner :select="mode" @change="mode=$event.target.select">
                        <m-spinner-item>直流功率</m-spinner-item>
                        <m-spinner-item>交流功率</m-spinner-item>
                    </m-spinner>
                    <m-card style="padding: 16px;margin-top: 16px;max-width: 100%">
                        <div v-show="mode==0?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td>电压：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="v1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>电流：</td>
                                    <td>
                                        <mtu-phyv unit="A" default="0" v-model="a1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>功率：</td>
                                    <td>
                                        <mtu-phyv unit="W" default="0" v-model="p1"></mtu-phyv>
                                    </td>
                                </tr>
                            </table>
                            <div>
                                <m-button @click="p1=v1*a1">计算功率</m-button>
                                <m-button @click="if(a1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{v1=p1/a1}">计算电压</m-button>
                                <m-button @click="if(v1==0){window.Mtu.dialog({title:'错误',message:'请检查输入',positive:'确定'})}else{a1=p1/v1}">计算电流</m-button>
                            </div>
                        </div>
                        <div v-show="mode==1?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td>电压(RMS)：</td>
                                    <td>
                                        <mtu-phyv unit="V" default="0" v-model="v2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>电流(RMS)：</td>
                                    <td>
                                        <mtu-phyv unit="A" default="0" v-model="a2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>cosφ：</td>
                                    <td>
                                        <m-edit-input :value="f2" @input="f2=$event.target.value" style="width:100%"></m-edit-input>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <m-button @click="s2=v2*a2;p2=v2*a2*f2;q2=Math.sqrt(s2*s2-p2*p2)">计算功率</m-button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>有功功率：</td>
                                    <td>
                                        <mtu-phyv unit="W" default="0" v-model="p2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>无功功率：</td>
                                    <td>
                                        <mtu-phyv unit="VAr" default="0" v-model="q2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>视在功率：</td>
                                    <td>
                                        <mtu-phyv unit="W" default="0" v-model="s2"></mtu-phyv>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});