routes.push({
    path:"/ne555",
    meta:{
        title:"NE555频率计算器"
    },
    component:{
        data:function(){
            return {
                type:1,

                r11:1000,
                r21:1000,
                c1:0.000047,
                t11:0,
                t21:0,
                t1:0,
                f1:0,
                d1:0,

                f2:1000,
                d2:60,
                r12:0,
                r22:0,
                c2:0.000001
            }
        },
        created:function(){
            this.update1();
            this.update12();
        },
        methods:{
            update1:function(){
                this.t11=0.693*(this.r11+this.r21)*this.c1;
                this.t21=0.693*this.r21*this.c1;
                this.t1=0.693*((this.r11+2*this.r21)*this.c1);
                this.d1=parseFloat((100.0-((100.0*this.r21)/(this.r11+(2.0*this.r21)))).toFixed(2));
                this.f1=1/this.t1;
            },
            update12:function(){
                var f=this.f2; 
                var d=this.d2;
                var c=this.c2;
                if(d<50){
                    d=100-d;
                    this.d2=d;
                };
                var r2=(1-1/100*d)*1.44/(f*c);
                this.r22=r2;
                var r1=1.44/(f*c)-2*r2;
                this.r12=r1;
            },
            update22:function(){
                var f=this.f2; 
                var r1=this.r12;
                var r2=this.r22;
                this.c2=1.44/(f*(r1+2*r2));
            }
        },
        template:`<div>
            <m-scrollbar style="height:100%">
                <m-appbar theme="dark" style="position:fixed;width:100%">
                    <router-link to="/" tag="m-button" slot="nav" mode="icon" theme="flat">
                        <m-icon value="arrow_back"></m-icon>
                    </router-link>
                    <div slot="title">NE555频率计算器</div>
                </m-appbar>
                <div style="height:56px"></div>
                <!-- 在这里定义应用程序 -->
                <div class="applet-container">
                    <m-card style="padding: 16px;margin-bottom:16px;max-width: 100%">
                        <div style="width:100%">
                            <img style="width:70%" src="images/ne555.png">
                        </div>
                        <m-tab :select="type-1" style="background-color:white">
                            <m-tab-item @click="type=1" style="text-transform:none">电路输出求值</m-tab-item>
                            <m-tab-item @click="type=2" style="text-transform:none">电路元件求值</m-tab-item>
                        </m-tab>
                        <div v-show="type==1?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td>R1：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="3" v-model="r11"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>R2：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="3" v-model="r21"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>C：</td>
                                    <td>
                                        <mtu-phyv unit="F" default="-6" v-model="c1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <m-button @click="update1()">计算</m-button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>T<sub>1</sub>上升沿：</td>
                                    <td>
                                        <mtu-phyv unit="s" default="0" v-model="t11"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>T<sub>2</sub>下升沿：</td>
                                    <td>
                                        <mtu-phyv unit="s" default="0" v-model="t21"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>T 总周：</td>
                                    <td>
                                        <mtu-phyv unit="s" default="0" v-model="t1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>f 频率：</td>
                                    <td>
                                        <mtu-phyv unit="Hz" default="0" v-model="f1"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>占空比：<br>(>50%)</td>
                                    <td>
                                        <div>
                                            <m-edit-input :value="d1" @input="d1=parseFloat($event.target.value)" style="width:calc(100% - 104px)"></m-edit-input>
                                            <m-spinner style="border-bottom:none;width:100px">
                                                <m-spinner-item>%</m-spinner-item>
                                            </m-spinner>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div v-show="type==2?true:false">
                            <table style="width:100%">
                                <tr>
                                    <td>频率：</td>
                                    <td>
                                        <mtu-phyv unit="Hz" default="3" v-model="f2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>占空比：<br>(>50%)</td>
                                    <td>
                                        <div>
                                            <m-edit-input :value="d2" @input="d2=parseFloat($event.target.value)" style="width:calc(100% - 104px)"></m-edit-input>
                                            <m-spinner style="border-bottom:none;width:100px">
                                                <m-spinner-item>%</m-spinner-item>
                                            </m-spinner>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>R1：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="3" v-model="r12"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>R2：</td>
                                    <td>
                                        <mtu-phyv unit="Ω" default="3" v-model="r22"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td>C：</td>
                                    <td>
                                        <mtu-phyv unit="F" default="-6" v-model="c2"></mtu-phyv>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <m-button @click="update12()">计算电阻</m-button>
                                        <m-button @click="update22()">计算电容</m-button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </m-card>
                </div>
            </m-scrollbar>
        </div>`
    }
});