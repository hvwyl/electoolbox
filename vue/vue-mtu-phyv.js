//物理量输入组件
// unit：物理单位
// default：默认量级 填写默认的次幂数
// fix：默认精确度 填写小数点后保留位数
Vue.component("mtu-phyv",{
    props:{
        "value":{},
        "unit":{},
        "default":{},
        "fix":{
            default:12
        }
    },
    data:function(){
        return {
            R:0,
            Ru:0
        }
    },
    created:function(){
        switch(this.default){
            case "9":this.Ru=0;break;
            case "6":this.Ru=1;break;
            case "3":this.Ru=2;break;
            case "0":this.Ru=3;break;
            case "-3":this.Ru=4;break;
            case "-6":this.Ru=5;break;
            case "-9":this.Ru=6;break;
            case "-12":this.Ru=7;break;
            default:this.Ru=3;
        };
        this.R=parseFloat((this.value/Math.pow(10,9-3*this.Ru)).toFixed(this.fix));
    },
    watch:{
        value:function(){
            var R=parseFloat((this.value/Math.pow(10,9-3*this.Ru)).toFixed(this.fix));
            if(isNaN(R)){
                this.R="";
            }else{
                this.R=R;
            };
        },
        R:function(){
            this.value=this.R==""?0:parseFloat((this.R*Math.pow(10,9-3*this.Ru)).toFixed(this.fix));
            this.$emit('input',this.value);
        },
        Ru:function(){
            this.R=parseFloat((this.value/Math.pow(10,9-3*this.Ru)).toFixed(this.fix));
        }
    },
    template:`
        <div>
            <m-edit-input :value="R" @input="R=parseFloat($event.target.value)" @focus="$emit('focus')" style="width:calc(100% - 104px)"></m-edit-input>
            <m-spinner :select="Ru" @change="Ru=$event.target.select" style="border-bottom:none;width:100px">
                <m-spinner-item>G{{unit}}</m-spinner-item>
                <m-spinner-item>M{{unit}}</m-spinner-item>
                <m-spinner-item>k{{unit}}</m-spinner-item>
                <m-spinner-item>{{unit}}</m-spinner-item>
                <m-spinner-item>m{{unit}}</m-spinner-item>
                <m-spinner-item>μ{{unit}}</m-spinner-item>
                <m-spinner-item>n{{unit}}</m-spinner-item>
                <m-spinner-item>p{{unit}}</m-spinner-item>
            </m-spinner>
        </div>
    `
});