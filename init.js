new Vue({
    el: '#app',
    data:{},
    router:(function(){
        var router=new VueRouter({
            routes:routes,
            scrollBehavior:function(to,from,savedPosition){
                return {x:0,y:0};
            }
        });
        router.beforeEach(function(to,from,next){
            if(to.meta.title=="电路计算工具箱"){
                window.document.title="电路计算工具箱";
                next();
            }else{
                window.document.title="电路计算工具箱 - "+to.meta.title;
                next();
            };
        });
        return router;
    })(),
    template:`<router-view style="height:100%;width:100%;"></router-view>`
});